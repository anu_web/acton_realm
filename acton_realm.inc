<?php

/**
 * Displays the login page.
 */
function acton_realm_login_page() {
  // Redirect active realm.
  if (acton_realm_is_logged_in()) {
    $redirect = isset($_GET['destination']) ? $_GET['destination'] : '';
    drupal_goto($redirect);
  }

  // Display login form.
  if ($title = variable_get('acton_realm_login_page_title')) {
    drupal_set_title($title);
  }
  return drupal_get_form('acton_realm_login');
}

/**
 * Builds the login form.
 */
function acton_realm_login($form, &$form_state) {
  $form['username'] = array(
    '#type' => 'textfield',
    '#title' => variable_get('acton_realm_login_username_label') ?: t('Username'),
    '#required' => TRUE,
    '#description' => variable_get('acton_realm_login_username_description'),
  );
  $form['password'] = array(
    '#type' => 'password',
    '#title' => variable_get('acton_realm_login_password_label') ?: t('Password'),
    '#required' => TRUE,
    '#description' => variable_get('acton_realm_login_password_description'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Log in'),
  );

  return $form;
}

/**
 * Authenticates credentials against LDAP.
 */
function acton_realm_login_validate($form, &$form_state) {
  $username = $form_state['values']['username'];
  if (acton_logon_ldap_authenticate_ldap($username, $form_state['values']['password'])) {
    $form_state['acton_realm_uid'] = $username;
    if ($realms = acton_realm_fetch_user_realms($username)) {
      $form_state['acton_realm_realms'] = $realms;
    }
  }
  if (!isset($form_state['acton_realm_uid']) || !isset($form_state['acton_realm_realms'])) {
    $message = variable_get('acton_realm_login_error_message', '');
    form_error($form['username'], $message);
    form_error($form['password']);
  }
}

/**
 * Activates the realms for valid credentials.
 */
function acton_realm_login_submit($form, &$form_state) {
  if (isset($form_state['acton_realm_uid']) && isset($form_state['acton_realm_realms'])) {
    // Regenerate active realm session to prevent session fixation attack.
    drupal_session_regenerate();
    $_SESSION['acton_realm_uid'] = $form_state['acton_realm_uid'];
    acton_realm_set_active_realms($form_state['acton_realm_realms']);

    if (variable_get('acton_realm_login_watchdog')) {
      acton_realm_log_to_watchdog('login', $form_state['acton_realm_uid'], $form_state['acton_realm_realms']);
    }
  }

  // Redirect to front page by default.
  $form_state['redirect'] = NULL;
}

/**
 * Obtains user realms.
 */
function acton_realm_fetch_user_realms($username) {
  $uri = variable_get('acton_logon_ldap_uri');
  $binddn = variable_get('acton_logon_ldap_binddn');
  if (!empty($uri) && !empty($binddn) && $conn = @ldap_connect($uri)) {
    $dn = str_replace('%{username}', $username, $binddn);
    return acton_realm_resolve_ldap_user_realms($conn, $dn);
  }

  return array();
}

/**
 * Resolves realms from LDAP user data.
 */
function acton_realm_resolve_ldap_user_realms($ldap_connection, $dn) {
  // Load LDAP options.
  $realms = array();
  $ldap_attributes = array();
  $ldap_groups = array();
  $result = db_select('acton_realm', 'r')->fields('r', array('rid', 'conf'))->execute();
  foreach ($result->fetchAllKeyed() as $rid => $conf) {
    $conf = unserialize($conf);
    $conf = is_array($conf) ? $conf : array();
    $conf += array('ldap_auth_type' => 'user', 'ldap' => array(), 'ldap_group' => array());
    $conf['ldap'] += array('criteria' => array(), 'mode' => '');
    $conf['ldap_group'] += array('dn' => '', 'member_attribute' => '');
    $realms[$rid] = $conf + array('rid' => $rid);

    // Collect attributes for LDAP query.
    if ($conf['ldap_auth_type'] == 'user') {
      foreach ($realms[$rid]['ldap']['criteria'] as $criterion) {
        $ldap_attributes[$criterion['attribute']] = 1;
      }
    }
    // Collect LDAP groups.
    elseif ($conf['ldap_auth_type'] == 'group') {
      $ldap_groups[$rid] = $conf['ldap_group'];
    }
  }
  $ldap_attributes = array_keys($ldap_attributes);

  // Query LDAP groups.
  $groups = array();
  foreach ($ldap_groups as $rid => $ldap_group) {
    $group_filter = $ldap_group['member_attribute'] . '=' . $dn;
    if ($ldap_result = @ldap_read($ldap_connection, $ldap_group['dn'], $group_filter, array('dn'))) {
      $entries = @ldap_get_entries($ldap_connection, $ldap_result);
      if (!empty($entries['count'])) {
        $groups[$rid] = TRUE;
      }
      @ldap_free_result($ldap_result);
    }
  }

  // Query LDAP data for user.
  $data = array();
  if ($ldap_attributes && $ldap_result = @ldap_read($ldap_connection, $dn, 'objectClass=*', $ldap_attributes)) {
    // Extract data for valid entry.
    $entries = @ldap_get_entries($ldap_connection, $ldap_result);
    if (!empty($entries['count']) && !empty($entries[0]['count'])) {
      // Extract data.
      foreach ($ldap_attributes as $attribute) {
        if (!empty($entries[0][$attribute]['count'])) {
          for ($i = 0; $i < $entries[0][$attribute]['count']; $i ++) {
            $data[$attribute][] = $entries[0][$attribute][$i];
          }
        }
      }
    }
    @ldap_free_result($ldap_result);
  }

  if ($groups || $data) {
    // Evaluate data against realms.
    $operators = acton_realm_ldap_operators();
    $evaluation = array_map(function ($realm) use ($groups, $data, $operators) {
      if ($realm['ldap_auth_type'] == 'user') {
        return array_reduce($realm['ldap']['criteria'], function ($carry, $criterion) use ($realm, $data, $operators) {
          $operator = $operators[$criterion['operator']];
          $values = isset($data[$criterion['attribute']]) ? $data[$criterion['attribute']] : array();
          return $realm['ldap']['mode'] == 'all'
            ? $carry && $operator($values, $criterion['value'])
            : $carry || $operator($values, $criterion['value']);
        }, $realm['ldap']['mode'] == 'all');
      }
      elseif ($realm['ldap_auth_type'] == 'group') {
        return !empty($groups[$realm['rid']]);
      }
      else {
        return FALSE;
      }
    }, $realms);
    return array_keys(array_filter($evaluation));
  }

  return array();
}

/**
 * Menu callback; attempts to log out the user.
 */
function acton_realm_logout() {
  if (user_is_logged_in()) {
    unset($_GET);
    drupal_goto('user/logout');
  }
  else {
    if (variable_get('acton_realm_login_watchdog') && isset($_SESSION['acton_realm_uid'])) {
      acton_realm_log_to_watchdog('logout', $_SESSION['acton_realm_uid']);
    }
    acton_realm_reset_active_realms();
    drupal_goto();
  }
}

/**
 * Returns functions for valid LDAP attribute value operators.
 */
function acton_realm_ldap_operators() {
  return array(
    '!=' => function ($values, $test) {
      return !in_array($test, $values);
    },
    '=' => function ($values, $test) {
      return in_array($test, $values);
    },
    '!~' => function ($values, $test) {
      $pattern = '/' . str_replace('/', '\/', $test) . '/';
      return array_reduce($values, function ($carry, $value) use ($test, $pattern) {
        return $carry && !preg_match($pattern, $value);
      }, TRUE);
    },
    '~' => function ($values, $test) {
      $pattern = '/' . str_replace('/', '\/', $test) . '/';
      return array_reduce($values, function ($carry, $value) use ($test, $pattern) {
        return $carry || preg_match($pattern, $value);
      }, FALSE);
    },
  );
}
