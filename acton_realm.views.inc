<?php

/**
 * Implements hook_views_data().
 */
function acton_realm_views_data() {
  $data['acton_realm']['table']['group'] = t('Realm');
  $data['acton_realm']['table']['base'] = array(
    'field' => 'rid',
    'title' => t('Realm'),
    'help' => t('Realm configuration.'),
    'weight' => -10,
  );

  $data['acton_realm']['rid'] = array(
    'title' => t('ID'),
    'help' => t('Numeric identifier for the realm configuration.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter',
    ),
  );

  $data['acton_realm']['title'] = array(
    'title' => t('Title'),
    'help' => t('User-friendly realm label.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['acton_realm']['weight'] = array(
    'title' => t('Weight'),
    'help' => t('Ordering weight.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['acton_realm_node']['table']['group']  = t('Realm');
  $data['acton_realm_node']['table']['join'] = array(
    'acton_realm' => array(
      'left_field' => 'rid',
      'field' => 'rid',
    ),
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  return $data;
}

/**
 * Implements hook_views_data_alter().
 */
function acton_realm_views_data_alter(&$data) {
  $types = array_values(array_filter(variable_get('acton_realm_field_node_types', array())) ?: array());
  $data['node']['acton_realm_rid'] = array(
    'title' => t('Realm'),
    'help' => t('Relate nodes to realms.'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'label' => t('Realm'),
      'base' => 'acton_realm',
      'relationship table' => 'acton_realm_node',
      'relationship field' => 'rid',
      'extra' => array(
        array(
          'table' => 'node',
          'field' => 'type',
          'value' => $types,
        ),
      ),
    ),
  );
}
