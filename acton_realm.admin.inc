<?php

function acton_realm_list() {
  $output = '<p>' . t('Public option label: %realm', array('%realm' => variable_get('acton_realm_field_public_option'))) . '</p>';

  $table['header'] = array(
    t('Realm'),
    array(
      'data' => t('Operations'),
      'colspan' => 2,
    ),
  );
  $table['empty'] = t('There are no realms to display.');
  foreach (acton_realm_get_options() as $rid => $title) {
    $table['rows'][] = array(
      $title,
      l(t('edit'), 'admin/config/system/acton-realm/edit/' . $rid),
      l(t('delete'), 'admin/config/system/acton-realm/delete/' . $rid),
    );
  }

  $table['attributes']['style'] = 'width: auto; min-width: 500px';
  $output .= theme('table', $table);

  return $output;
}

function acton_realm_edit($form, $form_state, $realm = FALSE) {
  form_load_include($form_state, 'inc', 'acton_realm');

  if (!$realm) {
    $realm = array(
      'rid' => NULL,
      'title' => '',
      'conf' => array(),
      'weight' => 0,
    );
  }
  $form['#realm'] = $realm;

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#maxlength' => 255,
    '#default_value' => $realm['title'],
    '#required' => TRUE,
    '#description' => t('The user-friendly label for this realm. This title is also used as the option label in the realm field.'),
  );
  $form['weight'] = array(
    '#type' => 'textfield',
    '#title' => t('Weight'),
    '#size' => 5,
    '#default_value' => $realm['weight'],
    '#required' => TRUE,
    '#description' => t('Realms with higher weights sink towards the bottom. Realms with equal weights are ordered by title.'),
    '#element_validate' => array('element_validate_integer'),
  );
  $form['conf'] = array(
    '#tree' => TRUE,
  );

  $realm['conf'] += array(
    'access_check' => array(),
    'ldap_auth_type' => 'user',
    'ldap' => array(),
    'ldap_group' => array(),
  );

  $realm['conf']['access_check'] += array('denied_title' => '', 'denied_message' => '', 'redirect' => TRUE);
  $form['conf']['access_check'] = array(
    '#type' => 'fieldset',
    '#title' => t('Access check options'),
  );
  $form['conf']['access_check']['denied_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Access denied page title'),
    '#default_value' => $realm['conf']['access_check']['denied_title'],
    '#description' => t('Custom page title for the "Access denied" page. Access is denied when the user has not authenticated or is not in an authorized realm.'),
  );
  $form['conf']['access_check']['denied_message'] = array(
    '#type' => 'textarea',
    '#title' => t('Access denied message'),
    '#default_value' => $realm['conf']['access_check']['denied_message'],
    '#description' => t('Custom HTML message for customizing the "Access denied" page.'),
  );
  $form['conf']['access_check']['redirect'] = array(
    '#type' => 'checkbox',
    '#title' => t('Redirect denied access to login page'),
    '#default_value' => $realm['conf']['access_check']['redirect'],
    '#description' => t('Redirect the unidentified user to the login page when the user attempts to access a page in this realm.'),
  );

  $form['conf']['ldap_auth_type'] = array(
    '#type' => 'radios',
    '#title' => t('Type of LDAP authorisation'),
    '#options' => array(
      'user' => t('User-based validation'),
      'group' => t('Group-based membership validation'),
    ),
    '#default_value' => $realm['conf']['ldap_auth_type'],
    '#attributes' => array(
      'class' => array($ldap_type_id = drupal_html_id('acton_realm_ldap_auth_type')),
    ),
    '#required' => TRUE,
  );
  $state_type_is_user = array(".{$ldap_type_id}" => array('value' => 'user'));
  $state_type_is_group = array(".{$ldap_type_id}" => array('value' => 'group'));

  $realm['conf']['ldap'] += array('criteria' => array(), 'mode' => 'any');
  $form['conf']['ldap'] = array(
    '#type' => 'fieldset',
    '#title' => t('LDAP user validation'),
    '#description' => t('Configure LDAP settings for classifying a user to this realm.'),
    '#states' => array(
      'visible' => $state_type_is_user,
    ),
  );
  $form['conf']['ldap']['config_msg'] = array(
    '#prefix' => '<p>',
    '#markup' => t('LDAP connection settings are configured in !acton_logon_link. Note that the "Directory bind DN" setting is also used as LDAP user data search base.', array(
      '!acton_logon_link' => l(t('Acton Logon LDAP'), 'admin/config/people/acton-logon/ldap'),
    )),
    '#suffix' => '</p>',
  );
  $form['conf']['ldap']['criteria'] = array(
    '#type' => 'textarea',
    '#title' => t('LDAP data criteria'),
    '#default_value' => implode("\n", array_map(function ($criterion) {
      return "$criterion[attribute]$criterion[operator]$criterion[value]";
    }, $realm['conf']['ldap']['criteria'])),
    '#description' => t('Enter LDAP attributes to match, one per line, in the format of: <strong>attribute</strong>=<strong>value</strong>
        <br>Valid operators include: @operators', array('@operators' => implode(' ', array_keys(acton_realm_ldap_operators())))),
    '#attributes' => array('style' => 'font-family: monospace; font-size: 110%;'),
    '#states' => array(
      'required' => $state_type_is_user,
    ),
  );
  $form['conf']['ldap']['mode'] = array(
    '#type' => 'radios',
    '#title' => t('Mode'),
    '#options' => array(
      'any' => t('Match any criterion'),
      'all' => t('Match all criteria'),
    ),
    '#default_value' => $realm['conf']['ldap']['mode'],
  );

  $realm['conf']['ldap_group'] += array(
    'dn' => '',
    'member_attribute' => 'uniqueMember',
  );
  $form['conf']['ldap_group'] = array(
    '#type' => 'fieldset',
    '#title' => t('LDAP group validation'),
    '#description' => t('Configure LDAP settings for classifying a user to this realm.'),
    '#states' => array(
      'visible' => $state_type_is_group,
    ),
  );
  $form['conf']['ldap_group']['dn'] = array(
    '#type' => 'textfield',
    '#title' => t('Group DN'),
    '#default_value' => $realm['conf']['ldap_group']['dn'],
    '#states' => array(
      'required' => $state_type_is_group,
    ),
  );
  $form['conf']['ldap_group']['member_attribute'] = array(
    '#type' => 'textfield',
    '#title' => t('Group member attribute'),
    '#default_value' => $realm['conf']['ldap_group']['member_attribute'],
    '#states' => array(
      'required' => $state_type_is_group,
    ),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

function acton_realm_edit_validate($form, &$form_state) {
  if ($form_state['values']['conf']['ldap_auth_type'] == 'user') {
    if (empty($form_state['values']['conf']['ldap']['criteria'])) {
      form_error($form['conf']['ldap']['criteria'], t('LDAP attribute criteria cannot be empty.'));
    }
  }
  elseif ($form_state['values']['conf']['ldap_auth_type'] == 'group') {
    if (empty($form_state['values']['conf']['ldap_group']['dn'])) {
      form_error($form['conf']['ldap_group']['dn'], t('Group DN cannot be empty.'));
    }
    if (empty($form_state['values']['conf']['ldap_group']['member_attribute'])) {
      form_error($form['conf']['ldap_group']['member_attribute'], t('Group member attribute must be set.'));
    }
  }

  if (isset($form_state['values']['conf']['ldap']['criteria'])) {
    $operators = array_keys(acton_realm_ldap_operators());
    $ldap_criteria = $form_state['values']['conf']['ldap']['criteria'];
    $lines = array_filter(preg_split('/[\r\n]/', $ldap_criteria), 'strlen');
    $criteria = array();
    foreach ($lines as $line) {
      $operator = NULL;
      foreach ($operators as $operator_candidate) {
        if (strpos($line, $operator_candidate)) {
          $operator = $operator_candidate;
          break;
        }
      }
      if (!$operator) {
        form_set_error('conf][ldap][criteria', t('Please enter a valid LDAP criteria list.'));
        break;
      }
      list($attribute, $value) = explode($operator, $line, 2);
      $criteria[] = compact('attribute', 'operator', 'value');
    }
    if ($criteria) {
      form_set_value($form['conf']['ldap']['criteria'], $criteria, $form_state);
    }
  }
}

function acton_realm_edit_submit($form, &$form_state) {
  $realm = $form['#realm'];
  $realm['title'] = $form_state['values']['title'];
  $realm['weight'] = $form_state['values']['weight'];
  $realm['conf'] = $form_state['values']['conf'];
  if ($realm['rid']) {
    drupal_write_record('acton_realm', $realm, 'rid');
  }
  else {
    drupal_write_record('acton_realm', $realm);
  }

  drupal_set_message(t('The realm has been successfully saved.'));
  $form_state['redirect'] = 'admin/config/system/acton-realm';
}

function acton_realm_delete($form, &$form_state, $realm) {
  $form['#realm'] = $realm;
  return confirm_form($form, t('Do you really want to delete this realm?'), 'admin/config/system/acton-realm',
    t('All content associated with this realm will no longer have a realm.'));
}

function acton_realm_delete_submit($form, &$form_state) {
  db_query('DELETE r, n FROM {acton_realm} r LEFT JOIN {acton_realm_node} n ON r.rid = n.rid
    WHERE r.rid = :rid', array(':rid' => $form['#realm']['rid']));
  drupal_set_message(t('The realm has been deleted.'));
  $form_state['redirect'] = 'admin/config/system/acton-realm';
}

function acton_realm_settings() {
  $form['field'] = array(
    '#type' => 'fieldset',
    '#title' => t('Field'),
  );
  $form['field']['acton_realm_field_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Field label'),
    '#default_value' => variable_get('acton_realm_field_label'),
    '#required' => TRUE,
    '#description' => t('Label for the access realm field in the node edit form.'),
  );
  $form['field']['acton_realm_field_description'] = array(
    '#type' => 'textfield',
    '#title' => t('Field description'),
    '#default_value' => variable_get('acton_realm_field_description'),
    '#description' => t('Description for the access realm field.'),
  );
  $form['field']['acton_realm_field_public_option'] = array(
    '#type' => 'textfield',
    '#title' => t('Public option label'),
    '#default_value' => variable_get('acton_realm_field_public_option'),
    '#description' => t('Label for the access realm field in the node edit form. If left blank, the public realm is not selectable (i.e. all content must belong to a realm when saved).'),
  );
  $form['field']['acton_realm_field_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Enabled content types'),
    '#default_value' => variable_get('acton_realm_field_node_types', array()),
    '#options' => node_type_get_names(),
  );

  $form['login'] = array(
    '#type' => 'fieldset',
    '#title' => t('Login form and page'),
  );
  $form['login']['acton_realm_login_page_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Login page title'),
    '#default_value' => variable_get('acton_realm_login_page_title'),
    '#description' => t('Defaults to "Log in" if left empty.'),
  );
  $form['login']['acton_realm_login_username_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Username input label'),
    '#default_value' => variable_get('acton_realm_login_username_label'),
    '#description' => t('Defaults to "Username" if left empty.'),
  );
  $form['login']['acton_realm_login_username_description'] = array(
    '#type' => 'textfield',
    '#title' => t('Username input description'),
    '#default_value' => variable_get('acton_realm_login_username_description'),
  );
  $form['login']['acton_realm_login_password_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Password input label'),
    '#default_value' => variable_get('acton_realm_login_password_label'),
    '#description' => t('Defaults to "Password" if left empty.'),
  );
  $form['login']['acton_realm_login_password_description'] = array(
    '#type' => 'textfield',
    '#title' => t('Password input description'),
    '#default_value' => variable_get('acton_realm_login_password_description'),
  );
  $form['login']['acton_realm_login_error_message'] = array(
    '#type' => 'textfield',
    '#title' => t('Error message'),
    '#default_value' => variable_get('acton_realm_login_error_message'),
    '#description' => t('The error message will show if the credentials are incorrect or do not match any realm.'),
  );

  $form['acton_realm_login_watchdog'] = array(
    '#type' => 'checkbox',
    '#title' => t('Log realm authorization'),
    '#default_value' => variable_get('acton_realm_login_watchdog'),
  );

  $form['#validate'][] = 'acton_realm_settings_validate';
  return system_settings_form($form);
}

function acton_realm_settings_validate() {
  if (module_exists('views')) {
    views_invalidate_cache();
  }
}
